# GIN docker image

A docker compose image that runs the GIN repository together with additional services, including:

- GIN-WEB (`gnode/gin-web`): the web ui for the repository
- MySQL (`mysql:5.6`): persistent data storage for the GIN repository 
- Drone (`drone/drone`): microservice adding pipelines into GIN repository
- GIN-INDEX (`gnode/gin-dex`): microservice that indexes the data and commits for searching
- ElasticSearch (`docker.elastic.co/elasticsearch/elasticsearch:6.8.8`): used to store the indexed data for GIN-INDEX

## Setup

Set the appropriate parameters:
- the `app.ini` contains settings for the GIN-web service
- the `.env` file contains environment variables for the docker compose services

> **_NOTE:_**  Make sure that the `GIN_INDEX_KEY` in .env and `search.SEARCH_KEY` in app.ini match for the indexing service to work properly

To start the GIN image, run the `start.sh` script. Use the `stop.sh` to stop the image.

Alternatively, you can run the image with command `docker-compose up -d` in the directory containing the `docker-compose.yml` file.

> **_NOTE:_**  The Elasticsearch container requires the kernel setting `vm.max_map_count` to be at least `262144`. Make sure you have set that on the docker host before running docker-compose (the `start.sh` script checks this for you). See [the Docs](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#_set_vm_max_map_count_to_at_least_262144) for more.

## Usage

For the provided settings, the GIN web service is reachable at an IP address `172.19.0.2:3000`. The Drone server runs at `172.19.0.3`.

The default user and password created for GIN is `ginadmin`. This user is allowed to login to Drone as well.

## Pipelines

The pipelines are maintained by the Drone service at `172.19.0.3`. 
Only the default admin user is allowed to login. 
A repository is set up for pipelines if it is `activated` in Drone and it's 
Project settings is set to `Trusted`. 

A secret key named `DRONE_PRIVATE_SSH_KEY` must be set in the Drone repository
 settings. The value of this key is a private ssh key. 
 The public key for this ssh key must be set for the user in the GIN repository. 
 This can be any generated ssh key, e.g., a RSA ssh key pair.

The repository in GIN has to contain a valid `.drone.yml` file in the 
root of the repository. An example of this file is [here](examples/.drone.yml) 

> **NOTE:** The `init-clone` step is not to be changed! 
>This clones the repository for the Drone runner into a shared volume, using the ssh key in the Drone's secrets. 
>
>User specified commands can be put into any following steps, that the user can create. The repository data are 
>accesible at the `$DRONE_REPO_NAME` directory by mounting the shared volume 
>for that step
```
steps:
  - name: init-clone
    ... DO NOT CHANGE THIS STEP
    
  - name: user-step
    image: alpine
    volumes:
      - name: repo
        path: /repo
    commands:
      - cd "$DRONE_REPO_NAME"
```