#!/usr/bin/env bash

VM_LIMIT=262144

vm_mmap=$(sysctl vm.max_map_count -n)
if [ "$vm_mmap" -lt $VM_LIMIT ]; then
  echo "Virtual memory map count limit too low. Setting to ${VM_LIMIT}"
  sudo sysctl -w vm.max_map_count=$VM_LIMIT
fi

if [ ! -d ./db/elasticsearch/data ]; then
  echo "Creating missing volume directories"
  mkdir -p ./db/elasticsearch/data
  chmod 777 ./db/elasticsearch/data
fi

echo "Starting the docker GIN image in background"
docker-compose up -d

echo "Initializing the default GIN admin user"
./docker/bash/wait-for-it.sh 172.19.0.2:3000

./docker/bash/wait-for-it.sh 172.19.0.5:5432 --strict -- docker exec -i -u postgres db-postgres psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname gin_gogs <<-EOSQL
  INSERT INTO "user" ("id","lower_name","name","full_name","email","passwd","login_type","login_source","login_name","type","location","website","rands","salt","created_unix","updated_unix","last_repo_visibility","max_repo_creation","is_active","is_admin","allow_git_hook","allow_import_local","prohibit_login","avatar","avatar_email","use_custom_avatar","num_followers","num_following","num_stars","num_repos","description","num_teams","num_members")
  VALUES (1,'ginadmin','ginadmin','GIN admin','ginadmin@kiv.zcu.cz','82434faec958a6f3b4efdbb1ad598e36604863800939d236f4736eb066fcdc905e4d12d6e56646a6d0fef00b391ddab8af43',0,0,'',0,'KIV ZCU','','EqwQL0TRFw','V9J7izPQPW',(extract(epoch from now()) * 1000),(extract(epoch from now()) * 1000),false,-1,true,true,false,false,false,'84010d196a00fc764252627102d356f8','ginadmin@kiv.zcu.cz',false,0,0,0,1,'',0,0)
  ON CONFLICT ON CONSTRAINT user_pkey DO UPDATE SET is_admin=true, updated_unix=(extract(epoch from now()) * 1000);
EOSQL
